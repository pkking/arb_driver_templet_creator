#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dir.h>
#include "file_data.h"
#include "templet_creator.h"
/**copy file to path, file_name contain the file path*/
static status copy_file(char* file_name, char* path)
{
    status error = SUCCESS;
    char *ptr;
    char new_path[PATH_LEN],col_buf[COLUMN_LEN];
    FILE *dest,*src;

    if(!(src = fopen(file_name, "rb")))
    {
        return error = FILENAME_INVAILD;
    }
    if((ptr = strstr(file_name,"ES7481.cpp")))
    {
        ptr = strrchr(strcpy(ptr,path),'\\');
        strcpy(ptr, CPLUSPLUS_SUFIX);
    }
    else if((ptr = strstr(file_name,"ES7481.h")))
    {
        ptr = strrchr(strcpy(ptr,path),'\\');
        strcpy(ptr, HEAD_SUFIX);
    }

    strcpy(new_path, path);
    strcat(new_path, file_name);

    if(!(dest = fopen(new_path, "wb")))
    {
        return error = CREATFILE_FAILED;
    }

    while(fgets(col_buf, COLUMN_LEN, src))
    {
        fputs(col_buf, dest);
    }

    fclose(src);
    fclose(dest);

    return error;
}

status templet_init(struct list_head *h,struct list_head *dir, prefix name)
{
    status error = SUCCESS;
    node tmp;
//    char del = 0;
    struct list_head *ptr;
    char path[PATH_LEN] = ".\\",path_tmp[PATH_LEN];
    char src_file_name[FILENAME_LEN];
//init dir
    mkdir(name);
    strcat(strcat(path, name),"\\");

    file_data_init(h,dir);
    list_for_each(ptr, dir)
    {
        strcpy(path_tmp, path);
        tmp = list_entry(ptr, struct _file_node, list);
        strcat(path_tmp, tmp->file_name);
        mkdir(path_tmp);
    }

    memset(path, '\0', PATH_LEN);
    strcpy(path, ".\\");

//copy file into dir
    strcat(strcat(path, name), "\\");
    list_for_each(ptr, h)
    {
        memset(src_file_name, '\0', FILENAME_LEN);
        tmp = list_entry(ptr, struct _file_node, list);
        strcat(strcpy(src_file_name, tmp->file_path), tmp->file_name);
        switch(copy_file(src_file_name, path))
        {
            case FILENAME_INVAILD:
                printf(".\\%s%s is not created !\n", name,src_file_name+1);
                break;
            case CREATFILE_FAILED:
                printf("can not create .\\%s%s !\n", name,src_file_name+1);
                break;
            default:
                break;
        }
    }

    return error;
}



status templet_prefix_replace(char *file_path, char *file_name, prefix new_key, int mode)//file_path 必须包含前后'\' file_name必须包含后缀名
{
    status error = SUCCESS;
    FILE *src_fp, *new_fp;
    int buf_len;
    char *ptr,*start,*write_ptr;
    char path_buf[FILENAME_LEN],name_buf[FILENAME_LEN],buf[COLUMN_LEN],new_file_name[FILENAME_LEN] =".\\", write_buf[COLUMN_LEN],sufix[5];

    strcpy(name_buf, file_name);

    switch(mode)
    {
        case CPP_FILE:
            strcpy(sufix, CPLUSPLUS_SUFIX);
            break;
        case HEAD_FILE:
            strcpy(sufix, HEAD_SUFIX);
            break;
        case C_FILE:
            strcpy(sufix, C_SUFIX);
            break;
        default:
            return MODE_INVAILD;
    }

    strcpy(path_buf, file_path);
    src_fp = fopen(strcat(path_buf, name_buf), "rt");
    //memset(path_buf, '\0', FILENAME_LEN);
    strcpy(path_buf, file_path);
    if((ptr = strstr(name_buf, "ES7481")))
    {
        strcat(strcpy(ptr, new_key), sufix);
    }
    strcat(strcat(strcat(strcat(new_file_name, new_key), "\\"), file_path), name_buf);
    new_fp = fopen(new_file_name,"wt");

   // ptr = buf;
    while(NULL != (ptr = fgets(buf, COLUMN_LEN, src_fp)))
    {
        start = ptr;
        write_ptr = write_buf;

        while(NULL != (ptr = strstr(start,"ES7481")))
        {
            buf_len = ptr - start;
            memcpy(write_ptr, start, buf_len);
            write_ptr += buf_len;
            memcpy(write_ptr, new_key, strlen(new_key));
            write_ptr += strlen(new_key);
            start += strlen("ES7481") +buf_len;
        }
        memcpy(write_ptr, start, strlen(start)+1);

        fputs(write_buf, new_fp);
        memset(buf, '\0', COLUMN_LEN);
        memset(write_buf, '\0', COLUMN_LEN);
    }

    fclose(src_fp);
    fclose(new_fp);
    return error;
}

status templet_int_macros_modify(FILE macros_file, macros_name name , int key )
{
    status error = SUCCESS;

    return error;
}

status templet_str_macros_modify(FILE macros_file, macros_name name , char* key)
{
    status error = SUCCESS;

    return error;
}
