#ifndef TEMPLET_CREATOR_H_INCLUDED
#define TEMPLET_CREATOR_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

#define TRUE                             1
#define FALSE                            0
#define CPLUSPLUS_SUFIX                   ".cpp"
#define HEAD_SUFIX                        ".h"
#define C_SUFIX                           ".c"
#define CPP_FILE                         1
#define HEAD_FILE                        2
#define C_FILE                           3

//warning code is positive
//error code is negative
//success code is 0
#define SUCCESS                          0
#define OUT_OF_SPACE                    -1
#define FIND_NOTHING                    -2
#define FILENAME_INVAILD                -3
#define MACROSNAME_INVAILD              -4
#define CREATFILE_FAILED                -5
#define LACK_FILE                       -6
#define LACK_DLL                        -7
#define MODE_INVAILD                    -8

#define COMMAND_LEN                     20
#define FILENAME_LEN                    50
#define PATH_LEN                        50
#define COLUMN_LEN                     100

typedef int status;
typedef char* prefix;
typedef char* macros_name;
typedef char* key_word;


status templet_init(struct list_head *h,struct list_head *dir, prefix name);
status templet_prefix_replace(char *file_path, char *file_name, prefix new_key, int mode);
status templet_int_macros_modify(FILE macros_file, macros_name name , int key );
status templet_str_macros_modify(FILE macros_file, macros_name name , char* key);

#endif // TEMPLET_CREATOR_H_INCLUDED
