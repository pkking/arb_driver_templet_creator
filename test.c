#include <stdio.h>
#include <string.h>
#include "templet_creator.h"
#include <dir.h>
#include "file_data.h"

int main()
{
    node tmp;
    struct list_head *ptr;
    struct list_head *head = (struct list_head*)malloc(sizeof(struct list_head));
    struct list_head *dir = (struct list_head*)malloc(sizeof(struct list_head));
    FILE *fp = NULL;
    char prefix_name[FILENAME_LEN],path[PATH_LEN] = ".\\";
    int sufix;

    printf("input the new prefix:\n");
    scanf("%s",prefix_name);

    templet_init(head, dir, prefix_name); //生成目标目录，目标文件

    list_for_each(ptr, head)
    {
        memset(path, '\0', PATH_LEN);
        tmp = list_entry(ptr, struct _file_node, list);
        strcat(strcat(strcat(path, prefix_name), "\\"), tmp->file_name);
        fp = fopen(path,"rt"); //读取目标根目录下的模板文件

        if(!(sufix = file_sufix_of(tmp->file_name)))
        {
            continue;
        }

        templet_prefix_replace(tmp->file_path, tmp->file_name, prefix_name, sufix); //产生模板文件
    }
    file_name_free(head, dir);
    printf("done!\n");
    /*strcpy(path_tmp, path);
    if(!(cpp_fp = fopen(strcat(path_tmp, CPLUSPLUS_SUFIX),"rt")))
    {
        printf("cpp file do not exist!\n");
    }
    else
    {
        memset(path_tmp, '\0', PATH_LEN);
        strcpy(path_tmp, path);
        if((!(head_fp = fopen(strcat(path_tmp, HEAD_SUFIX),"rt"))))
        {
            printf("head file do not exist!\n");
        }
        else
        {
            templet_init(head, dir, prefix_name);
            templet_prefix_replace(cpp_fp, prefix_name, CPP_FILE);
            templet_prefix_replace(head_fp, prefix_name, HEAD_FILE);

            file_name_free(head, dir);

            printf("done!\n");
        }
    }*/
    system("pause");
    return 0;
}
