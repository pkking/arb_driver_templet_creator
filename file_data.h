#ifndef FILE_DATA_H_INCLUDED
#define FILE_DATA_H_INCLUDED

#include "list.h"
#include "templet_creator.h"

typedef struct _file_node* node;
struct _file_node
{
    struct list_head list;
    char file_name[FILENAME_LEN]; //文件名包含文件名、后缀名
    char file_path[PATH_LEN]; //文件路径为相对路径
};

int file_name_del(struct list_head *h, char *name);
int file_sufix_of(char *file_name);
int file_name_add(struct list_head *h, char *name, char *path);
int file_data_init(struct list_head *h, struct list_head *dir);
void file_name_free(struct list_head *h, struct list_head *dir);

#endif // FILE_DATA_H_INCLUDED
