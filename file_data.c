#include "file_data.h"
#include "templet_creator.h"
#include <dir.h>

int file_name_add(struct list_head *h, char *name, char *path)
{
    node tmp = (node)malloc(sizeof(struct _file_node));

    if(!tmp)
    {
        return OUT_OF_SPACE;
    }
    list_add_tail(&tmp->list, h);
    strcpy(tmp->file_name, name);
    strcpy(tmp->file_path, path);

    return SUCCESS;
}

int file_name_del(struct list_head *h, char *name)
{
    return SUCCESS;
}
/**释放文件名、目录链表*/
void file_name_free(struct list_head *h, struct list_head *dir)
{
    struct list_head *ptr,*tmp;

    list_for_each_safe(ptr, tmp, h)
    {
        free(list_entry(ptr, struct _file_node, list));
    }

    list_for_each_safe(ptr, tmp, dir)
    {
        free(list_entry(ptr, struct _file_node, list));
    }
}

/**初始化目录链表**/
int dir_data_init(struct list_head *h)
{
    INIT_LIST_HEAD(h);
    file_name_add(h, ".\\","");
    file_name_add(h, ".\\expat\\","");
    file_name_add(h, ".\\ivifgen\\","");

    return SUCCESS;
   // list_add_tail()
}


/** 初始化文件名链表 文件名包含文件名、后缀名**/
int file_data_init(struct list_head *h, struct list_head *dir)
{
    long file_handle;
    INIT_LIST_HEAD(h);
    struct _finddata_t tmp;
    char root_path[PATH_LEN] = ".\\",expat_path[PATH_LEN] = ".\\expat\\",ivifgen_path[PATH_LEN] = ".\\ivifgen\\";
    char name_buf[FILENAME_LEN],path_buf[PATH_LEN];

    dir_data_init(dir);
//root
    if(-1 != (file_handle = _findfirst(".\\*.*",&tmp)))
    {
        memset(path_buf, '\0', PATH_LEN);
        strcpy(path_buf, root_path);
        memset(name_buf,'\0',FILENAME_LEN);
        strcpy(name_buf, tmp.name);
        if(strcmp(name_buf, ".\\ES7481.cpp")&&strcmp(name_buf, ".\\ES7481.h")&&strcmp(name_buf, ".")
                &&strcmp(name_buf, "..")&&strcmp(name_buf, ".git")&&strcmp(name_buf, "arb_generator_templet_creator.exe"))
        {
            file_name_add(h, name_buf, path_buf);  //name_buf 为.\file_name.cpp
        }

        while(! _findnext(file_handle, &tmp))
        {
            memset(name_buf,'\0',FILENAME_LEN);
            memset(path_buf, '\0', PATH_LEN);
            strcpy(path_buf, root_path);
            strcpy(name_buf, tmp.name);
            if(strcmp(name_buf, ".\\ES7481.cpp")&&strcmp(name_buf, ".\\ES7481.h")&&strcmp(name_buf, ".")
                &&strcmp(name_buf, "..")&&strcmp(name_buf, ".git")&&strcmp(name_buf, "arb_generator_templet_creator.exe"))
            {
                file_name_add(h, name_buf, path_buf);  //name_buf 为.\file_name.cpp
            }
        }
    }

//dll expat
    if((file_handle = _findfirst(".\\expat\\*.*",&tmp) )!= -1L)
    {
        memset(path_buf, '\0', PATH_LEN);
        strcpy(path_buf, expat_path);
        memset(name_buf,'\0',FILENAME_LEN);
        strcpy(name_buf, tmp.name);
        if(strcmp(name_buf, ".")&&strcmp(name_buf, "..")&&strcmp(name_buf, ".git")&&strcmp(name_buf, "arb_generator_templet_creator.exe"))
        {
            file_name_add(h, name_buf, path_buf);
        }
        //dir_data_init(dir);


        while(!_findnext(file_handle, &tmp))
        {
            memset(name_buf,'\0',FILENAME_LEN);
            memset(path_buf, '\0', PATH_LEN);
            strcpy(path_buf, expat_path);
            strcpy(name_buf, tmp.name);
            if(strcmp(name_buf, ".")&&strcmp(name_buf, "..")&&strcmp(name_buf, ".git")&&strcmp(name_buf, "arb_generator_templet_creator.exe"))
            {
                file_name_add(h, name_buf, path_buf);
            }
        }
    }

//dll ivifgen
    if(-1 != (file_handle = _findfirst(".\\ivifgen\\*.*",&tmp)))
    {
        memset(path_buf, '\0', PATH_LEN);
        strcpy(path_buf, ivifgen_path);
        memset(name_buf,'\0',FILENAME_LEN);
        strcpy(name_buf, tmp.name);
        if(strcmp(name_buf, ".")&&strcmp(name_buf, "..")&&strcmp(name_buf, ".git")&&strcmp(name_buf, "arb_generator_templet_creator.exe"))
        {
            file_name_add(h, name_buf, path_buf);
        }
        //dir_data_init(dir);


        while(!_findnext(file_handle, &tmp))
        {
            memset(name_buf,'\0',FILENAME_LEN);
            memset(path_buf, '\0', PATH_LEN);
            strcpy(path_buf, ivifgen_path);
            strcpy(name_buf, tmp.name);
            if(strcmp(name_buf, ".")&&strcmp(name_buf, "..")&&strcmp(name_buf, ".git")&&strcmp(name_buf, "arb_generator_templet_creator.exe"))
            {
                file_name_add(h, name_buf, path_buf);
            }
        }
    }

    return 0;
}

int file_sufix_of(char *file_name) //文件名中只能有一个.
{
    char *ptr;

    if(!(ptr = strstr(file_name, ".")))
    {
        return FALSE;
    }
    if(!strcmp(ptr, CPLUSPLUS_SUFIX))
    {
        return CPP_FILE;
    }
    else if(!strcmp(ptr, HEAD_SUFIX))
    {
        return HEAD_FILE;
    }
    else if(!strcmp(ptr, C_SUFIX))
    {
        return C_FILE;
    }
    else
    {
        return FALSE;
    }
}
